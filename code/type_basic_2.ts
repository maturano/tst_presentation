let union: boolean | string;
union = 'false';

function someFunction(): void {};

// Null and Undefined
union = null; // No error

// Con --strictNullChecks
let lastName: string | null | undefined = null;

function rareFunction(): never {
    while (true) { }
    // throw new Error(message);
    // return error("Something failed");
}
