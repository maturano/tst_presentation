// Tuple type
let x: [string, number];

// Asignado un valor válido
x = ['hello', 10]; // OK

// Asignado un valor inválido
x = [10, 'hello']; // Error

x[0].substr(1); // OK
x[1].substr(1); // Error, 'number' does not have 'substr'
